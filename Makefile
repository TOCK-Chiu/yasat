CXX      = clang++
CXXFLAGS = -Wall -Wextra -O2 -ftree-vectorize -march=native -pedantic -flto \
           -fstack-protector-strong -std=c++1z
LDFLAGS  = -s -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now
LIBS     = -lz -llzma
TARGET   = yasat
SRCS     = $(wildcard *.cpp)
OBJS     = ${SRCS:%.cpp=%.o}

all: $(TARGET)

$(TARGET): $(OBJS)
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $^ -fPIE -pie -o $@ $(LIBS)

%.o: %.cpp
	$(CXX) $(CXXFLAGS) -fPIE $< -c -o $@

.PHONY: clean check

clean:
	$(RM) $(TARGET) *.o

check: all
	./check.sh 15m
