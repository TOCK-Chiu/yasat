#!/bin/bash
#set -x

if [ "$#" -ge 2 ]; then
	T_OUT="$1"
	BENCH="$2"
elif [ "$#" -ge 1 ]; then
	T_OUT="$1"
	BENCH='benchmarks'
else
	T_OUT='10s'
	BENCH='benchmarks'
fi

RED='\033[0;31m'
GRN='\033[0;32m'
YLW='\033[0;33m'
BLU='\033[0;34m'
NC='\033[0m'
TIME_TMP='/tmp/yasat_time'

TimeYasat() {

	TIME=$(TIMEFORMAT='%3R'; time (timeout "$T_OUT" ./yasat "$FILE" "$OUTPUT" > /dev/null 2>&1) 2>&1)
	echo "$? $TIME" > "$TIME_TMP"

	}

Cleanup() {

	rm -rf "$TIME_TMP"

	}

Terminate() {

	if [ ! -z "$CHILD" ]; then
		kill -s SIGKILL "$CHILD" > /dev/null 2>&1
	fi

	exit

	}

trap Cleanup EXIT
trap Terminate INT

while read FILE; do

	OUTPUT=${FILE%cnf}sat

	if [ ! -z "$(echo "$FILE" | grep UNSAT)" ]; then
		EXPT='UNSATISFIABLE'
	elif [ ! -z "$(echo "$FILE" | grep SAT)" ]; then
		EXPT='SATISFIABLE'
	else
		printf "[ ${YLW}SKIPPED${NC} ] -.--- s\t%s\n" "$FILE"
		continue
	fi

	TimeYasat &
	EPOCH=$(date +%s%3N)
	printf "[ ${BLU}RUNNING${NC} ] -.--- s\t%s\r" "$FILE"
	CHILD=$!

	while [ ! -z "$(ps -p "$CHILD" | grep "$CHILD")" ]; do
		DURATION=$(($(date +%s%3N)-$EPOCH))
		printf "[ ${BLU}RUNNING${NC} ] %s.%03d s\t%s\r" "$(($DURATION/1000))" "$(($DURATION%1000))" "$FILE"
		sleep 1
	done
	wait

	CHILD=''
	TS=$(cat "$TIME_TMP" | head -1 | awk '{ print $2 }')
	RET=$(cat "$TIME_TMP" | head -1 | awk '{ print $1 }')

	case "$RET" in
	139)
		printf "[ ${RED}SIGSEGV${NC} ] %s s\t%s\n" "$TS" "$FILE"
		;;
	124)
		printf "[ ${YLW}TIMEOUT${NC} ] %s s\t%s\n" "$TS" "$FILE"
		;;
	*)
		RES=$(cat "$OUTPUT" | head -1 | awk '{ print $2 }')

		if [ "$RES" == "$EXPT" ]; then
			printf "[ ${GRN}PASS${NC}    ] %s s\t%s\n" "$TS" "$FILE"
		else
			printf "[ ${RED}FAILED${NC}  ] %s s\t%s\n" "$TS" "$FILE"
		fi

		;;
	esac

done < <(find "$BENCH" -name '*.cnf' -type f)
