/*

  SAT.cpp

*/





#include <type_traits>
#include <limits>
#include <cstdlib>
#include <exception>
#include <stdexcept>
#include <sstream>
#include "SAT.hpp"



namespace {

class Extractor {

	std::istream& mbrIn;
	std::stringstream mbrSs;

public:
	Extractor(std::istream& argIn):
		mbrIn( argIn ) {
		;
		}

	operator bool(void) {

		return static_cast<bool>(mbrIn);

		}

	bool operator!(void) {

		return ! mbrIn;

		}

	template<class IN_TYPE>
	Extractor& operator>>(IN_TYPE& argTarget) {

		std::string locBuf;

		while ( (!(mbrSs >> argTarget)) && mbrSs.eof() && getline(mbrIn >> std::ws, locBuf) ) {

			if (locBuf.empty() || locBuf[0] == 'c')
				continue;

			mbrSs.clear();
			mbrSs.str(locBuf);

			}

		if ( ! mbrSs )
			mbrIn.setstate(std::ios::failbit);

		return *this;

		}

	};

} // anonymous namespace



bool satExpr::IsValidLiteral(satLit argLit) const noexcept {

	// I don't use the least elements of signed types, since such types
	// can't hold the additive inverses of these numbers.
	if constexpr (std::is_integral<satLit>::value && std::is_signed<satLit>::value) {

		if (argLit == std::numeric_limits<satLit>::min())
			return false;

		}

	if (argLit == 0 || static_cast<std::size_t>(abs(argLit)) > mbrNumOfVar)
		return false;

	return true;

	}



bool satExpr::IsValidClause(const satClause& argClause) const noexcept {

	for (auto locTerm : argClause) {

		if ( ! IsValidLiteral(locTerm) )
			return false;

		}

	return true;

	}



void satExpr::AddClause(const satClause& argClause) {

	if ( ! IsValidClause(argClause) )
		throw std::invalid_argument("satExpr::AddClause got bad clause");

	mbrExpr.emplace_back(argClause);

	}



void satExpr::AddClause(satClause&& argClause) {

	if ( ! IsValidClause(argClause) )
		throw std::invalid_argument("satExpr::AddClause got bad clause");

	mbrExpr.emplace_back( std::move(argClause) );

	}



bool satExpr::Verify(const satSol& argSol) const noexcept {

	if (argSol.size() != mbrNumOfVar)
		return false;

	for (const auto& locClause : mbrExpr) {

		bool locIsSatisfied = false;

		for (auto locTerm : locClause) {

			std::size_t locIndex = abs(locTerm) - 1;

			if (locIndex >= mbrNumOfVar)
				return false;

			if (argSol[locIndex] == locTerm) {

				locIsSatisfied = true;
				break;

				}

			}

		if (!locIsSatisfied)
			return false;

		}

	return true;

	}



std::ostream& operator<<(std::ostream& argOut, const satExpr& argExpr) {

	argOut << "p cnf " << argExpr.mbrNumOfVar << ' ' << argExpr.mbrExpr.size() << std::endl;

	for (const auto& locClause : argExpr.mbrExpr) {

		for (auto locTerm : locClause)
			argOut << locTerm << ' ';

		argOut << '0' << std::endl;

		}

	return argOut;

	}



std::istream& operator>>(std::istream& argIn, satExpr& argExpr) {

	Extractor locStream(argIn);
	argExpr.Clear();

	// Parse header
	{
		std::string locBuffer;

		if ( !((locStream >> locBuffer && locBuffer == "p") && (locStream >> locBuffer && locBuffer == "cnf")) ) {

			argIn.setstate(std::ios::failbit);
			return argIn;

			}

		}

	std::size_t locNumOfClause = 0;

	if ( ! (locStream >> argExpr.mbrNumOfVar >> locNumOfClause) )
		return argIn;

	satClause locClause;

	while (locNumOfClause-- > 0 && locStream) {

		satLit locLit = std::numeric_limits<satLit>::min();

		while (locStream >> locLit && argExpr.IsValidLiteral(locLit))
			locClause.emplace(locLit);

		if (locLit != 0) {

			argIn.setstate(std::ios::failbit);
			return argIn;

			}

		argExpr.mbrExpr.emplace_back( std::move(locClause) );

		}

	return argIn;

	}



satClause Resolve(const satClause& argClauseA,
                  const satClause& argClauseB,
                  satLit argLit) {

	satClause locResult(argClauseA);

	locResult.insert(argClauseB.begin(), argClauseB.end());
	locResult.erase(argLit);
	locResult.erase(-argLit);

	return locResult;

	}



satClause Resolve(satClause&& argClauseA,
                  const satClause& argClauseB,
                  satLit argLit) {

	argClauseA.insert(argClauseB.begin(), argClauseB.end());
	argClauseA.erase(argLit);
	argClauseA.erase(-argLit);

	return std::move(argClauseA);

	}



satClause Resolve(const satClause& argClauseA,
                  satClause&& argClauseB,
                  satLit argLit) {

	return Resolve(std::move(argClauseB), argClauseA, argLit);

	}



satClause Resolve(satClause&& argClauseA,
                  satClause&& argClauseB,
                  satLit argLit) {

	return Resolve(std::move(argClauseA), argClauseB, argLit);

	}
