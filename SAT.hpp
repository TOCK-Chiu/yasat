/*

  SAT.hpp

*/





#ifndef __SAT_HPP__
#define __SAT_HPP__

#if !defined(__cplusplus) || __cplusplus <= 201402L
#error "This file written in C++17"
#endif

#include <cstdint>
#include <vector>
#include <deque>
#include <unordered_set>
#include <istream>
#include <ostream>



using satLit = int;
using satClause = std::unordered_set<satLit>;
using satSol = std::vector<satLit>;

class satExpr {

	std::size_t mbrNumOfVar;
	std::deque<satClause> mbrExpr;

	// Range-based for, for satTransform
	auto begin() noexcept {

		return mbrExpr.begin();

		}

	auto end() noexcept {

		return mbrExpr.end();

		}

public:
	satExpr():
		mbrNumOfVar( 0 ) {
		;
		};

	satExpr(std::size_t argNumOfVar):
		mbrNumOfVar( argNumOfVar ) {
		;
		}

	satExpr(const satExpr& ) = default;
	satExpr(satExpr&& ) = default;
	satExpr& operator=(const satExpr& ) = default;
	satExpr& operator=(satExpr&& ) = default;

	void Init(std::size_t argNumOfVar) noexcept {

		mbrNumOfVar = argNumOfVar;
		mbrExpr.clear();

		}

	void Clear(void) noexcept {

		Init(0);

		}

	std::size_t NumOfVar(void) const noexcept {

		return mbrNumOfVar;

		}

	std::size_t NumOfClause(void) const noexcept {

		return mbrExpr.size();

		}

	// For range-based for, read-only
	auto begin() const noexcept {

		return mbrExpr.cbegin();

		}

	auto end() const noexcept {

		return mbrExpr.cend();

		}

	bool IsValidLiteral(satLit ) const noexcept;
	bool IsValidClause(const satClause& ) const noexcept;
	void AddClause(const satClause& );
	void AddClause(satClause&& );

	bool Verify(const satSol& ) const noexcept;

	friend std::ostream& operator<<(std::ostream& , const satExpr& );
	friend std::istream& operator>>(std::istream& , satExpr& );
	friend class satTransform;

	};



class satSolver {

	virtual satSol SolveImpl(const satExpr& ) const = 0;

public:
	virtual ~satSolver() {
		;
		}

	// If it can't find a solution, the returned value will be empty.
	satSol Solve(const satExpr& argExpr) const {

		return SolveImpl(argExpr);

		}

	};

class satTransform {

	virtual bool ApplyImpl(satExpr& argExpr) const = 0;

public:
	virtual ~satTransform() {
		;
		}

	// Return false if it can't be satisfied.
	bool Apply(satExpr& argExpr) const {

		return ApplyImpl(argExpr);

		}

	};



// Helper functions
std::ostream& operator<<(std::ostream& , const satExpr& );
std::istream& operator>>(std::istream& , satExpr& );

satClause Resolve(const satClause& , const satClause& , satLit );
satClause Resolve(satClause&& , const satClause& , satLit );
satClause Resolve(const satClause& , satClause&& , satLit );
satClause Resolve(satClause&& , satClause&& , satLit );



#endif
