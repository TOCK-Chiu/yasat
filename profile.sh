#!/bin/bash

if [ "$#" -ge 1 ]; then
	BENCHMARK="$1"
else
	BENCHMARK='benchmarks/milestone2/SAT/ii32a1.cnf'
fi

CPUPROFILE=/tmp/prof.out ./yasat "$BENCHMARK"
pprof --ps yasat /tmp/prof.out > prof.ps
convert prof.ps prof.png
rm /tmp/prof.out prof.ps
