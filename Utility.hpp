/*

  Utility.hpp

*/





#ifndef __UTILITY_HPP__
#define __UTILITY_HPP__

#if !defined(__cplusplus) || __cplusplus <= 201402L
#error "This file written in C++17"
#endif

#include <cstdlib>
#include <functional>
#include <zlib.h>
#include <lzma.h>



template<class T>
constexpr T GetAbsImpl(T Num) {

	T Tmp = (Num >> (sizeof(Num) * 8 - 1));

	return (Num ^ Tmp) - Tmp;

	}

template<class T>
constexpr unsigned int GetSignAbsImpl(T Num, T& Abs) {

	T Tmp = (Num >> (sizeof(Num) * 8 - 1));
	Abs = (Num ^ Tmp) - Tmp;

	return (Tmp & 1);

	}

template<class T>
T GetAbs(T Num) {

	if constexpr (GetAbsImpl(-5) == 5)
		return GetAbsImpl(Num);
	else
		return abs(Num);

	}

template<class T>
unsigned int GetSignAbs(T Num, T& Abs) {

	unsigned int Result = 0;

	if constexpr (GetAbsImpl(-5) == 5)
		Result = GetSignAbsImpl(Num, Abs);
	else
		Result = ((Abs = GetAbs(Num)) < 0) ? 1 : 0;

	return Result;

	}



template<class T, uLong INIT_CRC = 0>
struct CRC32 {

	std::size_t operator()(const T& Key) const noexcept {

		return crc32(INIT_CRC, reinterpret_cast<const Bytef*>(&Key), sizeof(Key));

		}

	};



template<class T, uint64_t INIT_CRC = 0>
struct CRC64 {

	std::size_t operator()(const T& Key) const noexcept {

		return lzma_crc64(reinterpret_cast<const uint8_t*>(&Key), sizeof(Key), INIT_CRC);

		}

	};



#endif
