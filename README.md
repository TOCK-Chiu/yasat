Introduction
============
YaSAT is yet another SAT solver.

Building
============
You need a compiler that supports C++17 to build YaSAT. As time being, only
[Clang](http://clang.llvm.org/) 3.9.0 suffices. Pre-built statically linked
binary is shipped along with source in case Clang 3.9.0 is not available to
you.

Usage
============
    ./yasat [<input> [output]]
If you don't specify output, YaSAT will output to stdout. If you don't specify
input either, YaSAT will read from stdin.

Implementation
============

Milestone 1
-----------
-   Simple preprocess
-   Two literal watching
-   BCP

Milestone 2
-----------
-   First UIP
-   A VSIDS scheme similar to MiniSAT

Milestone 3
-----------
-   Fix some bugs
-   Add preprocess (WIP)
