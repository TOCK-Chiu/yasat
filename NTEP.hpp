/*

  NTEP.hpp

*/





#ifndef __NTEP_HPP__
#define __NTEP_HPP__

#if !defined(__cplusplus) || __cplusplus <= 201402L
#error "This file written in C++17"
#endif

#include "SAT.hpp"



class NTEP : public satTransform {

	virtual bool ApplyImpl(satExpr& argExpr) const;

public:

	};



#endif
