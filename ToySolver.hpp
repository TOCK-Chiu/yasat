/*

  ToySolver.hpp

*/





#ifndef __TOY_SOLVER_HPP__
#define __TOY_SOLVER_HPP__

#if !defined(__cplusplus) || __cplusplus <= 201402L
#error "This file written in C++17"
#endif

#include "SAT.hpp"



class toySolver : public satSolver {

	static_assert(std::is_integral<satLit>::value && std::is_signed<satLit>::value,
	              "ToySolver needs to be written if satLit is not a sign integral type.");

	satSol SolveImpl(const satExpr& ) const;

public:

	};



#endif
