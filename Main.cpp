#include <iostream>
#include <memory>
#include <cstdio>
#include <cstring>
#include "NTEP.hpp"
#include "ToySolver.hpp"



int main(int ArgCount, const char* ArgVar[]) {

	if (ArgCount > 3 || (ArgCount == 2 && ! strcmp(ArgVar[1], "-h"))) {

		std::cerr << "\nYaSAT, yet another SAT solver\n\n"
		          << "Usage:\n"
		          << "  \'" << ArgVar[0] << "\' [<input> [output]]\n\n"
		          << "Options:\n"
		          << "  <input> ...... Input file path, or '-' for stdin.\n"
		          << "  <output> ..... Output file path, or '-' for stdout.\n"
		          << std::endl;
		return 1;

		}

	if (ArgCount > 1 && strcmp(ArgVar[1], "-") && freopen(ArgVar[1], "r", stdin) == nullptr) {

		std::cerr << "Failed to open input file." << std::endl;
		return 2;

		}

	if (ArgCount > 2 && strcmp(ArgVar[2], "-") && freopen(ArgVar[2], "w+", stdout) == nullptr) {

		std::cerr << "Failed to open output file." << std::endl;
		return 3;

		}

	try {

		satExpr Expression;

		std::cin >> Expression;

		if (!std::cin) {

			std::cerr << "YaSAT got bad input." << std::endl;
			return 4;

			}

		std::unique_ptr<satTransform> Preprocess(new NTEP);

		if (!Preprocess->Apply(Expression)) {

			std::cout << "s UNSATISFIABLE" << std::endl;
			return 0;

			}

		std::unique_ptr<satSolver> Solver(new toySolver);
		satSol Solution = Solver->Solve(Expression);

		if (Expression.NumOfVar() != Solution.size()) {

			std::cout << "s UNSATISFIABLE" << std::endl;
			return 0;

			}

		std::cout << "s SATISFIABLE" << std::endl << 'v';

		for (const auto Term : Solution)
			std::cout << ' ' << Term;

		std::cout << " 0" << std::endl;

		}
	catch (const std::exception& StdExcept) {

		std::cerr << "Exception caught: " << StdExcept.what() << std::endl;

		}
	catch (...) {

		std::cerr << "Caught unknown exception." << std::endl;

		}

	return 0;

	}
