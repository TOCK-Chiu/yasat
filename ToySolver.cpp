/*

  ToySolver.cpp

*/



#include <cassert>
#include <cstdlib>
#include <limits>
#include <vector>
#include <deque>
#include <set>
#include <unordered_set>
#include "Utility.hpp"
#include "ToySolver.hpp"



// Internal helper functions
namespace {

using tsHasher = std::hash<const satClause*>;
using litWatchList = std::unordered_set<const satClause*, tsHasher>;

using litWatch = struct {

	satLit Val = 0;
	const satClause* Antec = nullptr;
	long double VSIDS = 0;

	litWatchList Watch[2];

	};

using watchList = std::vector< litWatch >;
using assignSeq = std::deque< satLit >;
using assignLog = std::deque< assignSeq >;
using assignQueue = std::deque< std::pair<satLit, const satClause*> >;

auto fsComp = [](const litWatch* LHS, const litWatch* RHS) -> bool {

	return (LHS->VSIDS != RHS->VSIDS) ?
	       (LHS->VSIDS > RHS->VSIDS) :
	       (LHS < RHS);

	};

struct tsState {

	watchList mbrWatchList;
	assignLog mbrAssignLog;
	std::deque<satClause> mbrLearntClauses;

	// Branching related
	std::set<litWatch*, decltype(fsComp)> mbrFloppyVarSet;
	long double mbrBumpStride = 1.0l;
	static constexpr long double mbrBumpFactor = 1.06l;

	tsState(std::size_t initNumOfVar):
		mbrWatchList(initNumOfVar),
		mbrFloppyVarSet(fsComp) {

		for (std::size_t locLit = 0; locLit < initNumOfVar; locLit++)
			mbrFloppyVarSet.emplace(&mbrWatchList[locLit]);

		}

	// Branching decision related
	satLit GetFloppyVar(void) const noexcept;

	// Watch related
	bool WatchClause(const satClause& );
	void RemoveWatch(const satClause& );

	// Assign and BCP related
	satLit AssignValue(satLit , bool );
	bool UpdateWatch(assignQueue& , assignSeq& , satClause& );
	satLit FindNextLitToWatch(satLit , const satClause& );

	// Backtrack related
	void RevertAssignSeq(assignSeq& );
	satLit NonChronoBacktrack(const satClause& );

	// First-UIP
	satClause GenLearntClause(const satClause& , const assignSeq& );

	};

} // anonymous namespace



satLit tsState::GetFloppyVar(void) const noexcept {

	if (mbrFloppyVarSet.empty())
		return 0;

	return (*mbrFloppyVarSet.begin() - &mbrWatchList[0]) + 1;

	}



// Note: watched pointers shall not be clobbered!
bool tsState::WatchClause(const satClause& argClause) {

	std::size_t locNumOfWatch = 0;
	std::pair<satLit, litWatchList*> locWatch[2];

	for (auto locTerm : argClause) {

		// It can't be positive and negative in the same time cz
		// this is no quantum physics
		if (argClause.find(-locTerm) != argClause.end())
			return true;

		satLit       locAbs = 0;
		unsigned int locKind = GetSignAbs(locTerm, locAbs);

		auto& locListEntry = mbrWatchList[locAbs - 1];

		// If the term is evaluated to true
		if (locTerm == locListEntry.Val)
			return true;

		// If the term is evaluated to false
		if (locListEntry.Val != 0)
			continue;

		// If this term is not assigned yet
		std::size_t locNumOfErased = mbrFloppyVarSet.erase(&locListEntry);
		locListEntry.VSIDS += mbrBumpStride;

		if (locNumOfErased > 0)
			mbrFloppyVarSet.emplace(&locListEntry);

		if (locNumOfWatch < 2)
			locWatch[locNumOfWatch++] = {locTerm, &locListEntry.Watch[locKind]};

		}

	// This can lead to false negative if this function is not used carefully
	if (locNumOfWatch == 0)
		return false;

	if (locNumOfWatch == 1)
		return (AssignValue(locWatch[0].first, true) == 0);

	// No more magic here, add watch
	locWatch[0].second->emplace(&argClause);
	locWatch[1].second->emplace(&argClause);

	return true;

	}



void tsState::RemoveWatch(const satClause& argClause) {

	// TODO

	}



// Assign and BCP
satLit tsState::AssignValue(satLit argLit, bool argDropLog) {

	assignSeq   locAssignSeq;
	assignQueue locAssignQueue;

	locAssignQueue.emplace_back(argLit, nullptr);

	while ( locAssignQueue.size() ) {

		satClause locLearntClause;

		// Succeed
		if ( UpdateWatch(locAssignQueue, locAssignSeq, locLearntClause) )
			continue;

		RevertAssignSeq(locAssignSeq);

		if (locLearntClause.empty())
			return argLit;

		// Conflicting implication, non-chrono backtrack here
		satLit locFailedAssign = NonChronoBacktrack(locLearntClause);

		// Nothing to backtrack
		if (locFailedAssign == 0)
			return argLit;

		mbrBumpStride *= mbrBumpFactor;

		mbrLearntClauses.emplace_back(std::move(locLearntClause));

		// If we've learnt an unit clause here, then everything will be
		// popped out and the literal in this clause must be true
		if (!WatchClause(mbrLearntClauses.back()))
			return std::numeric_limits<satLit>::min();

		// FIXME
		if (mbrWatchList[GetAbs(locFailedAssign) - 1].Val)
			return 0;

		argLit = locFailedAssign;

		locAssignSeq.clear();
		locAssignQueue.clear();

		locAssignQueue.emplace_back(argLit, nullptr);

		}

	// Only preserve log if we are not preprocessing
	if (!argDropLog)
		mbrAssignLog.emplace_back(std::move(locAssignSeq));

	return 0;

	}



void tsState::RevertAssignSeq(assignSeq& argAssignSeq) {

	for (satLit locAssign : argAssignSeq) {

		auto& locListEntry = mbrWatchList[GetAbs(locAssign) - 1];

		locListEntry.Val = 0;
		locListEntry.Antec = nullptr;
		mbrFloppyVarSet.emplace(&locListEntry);

		}

	}



satLit tsState::NonChronoBacktrack(const satClause& argLearntClause) {

	// Failed during preprocess
	if (mbrAssignLog.empty())
		return 0;

	const satLit locBaseline = mbrAssignLog.front().front();
	satLit locFailedAssign = locBaseline;

	while (mbrAssignLog.size() && locFailedAssign == locBaseline) {

		for (satLit locAssign : mbrAssignLog.back()) {

			if (argLearntClause.find(-locAssign) != argLearntClause.end())
				locFailedAssign = mbrAssignLog.back().front();

			auto& locListEntry = mbrWatchList[GetAbs(locAssign) - 1];

			locListEntry.Val = 0;
			locListEntry.Antec = nullptr;
			mbrFloppyVarSet.emplace(&locListEntry);

			}

		mbrAssignLog.pop_back();

		}

	return locFailedAssign;

	}



satLit tsState::FindNextLitToWatch(satLit argRevLit, const satClause& argClause) {

	satLit locLit = 0;

	for (satLit locTerm : argClause) {

		if (locTerm == argRevLit)
			continue;

		satLit       locAbs = 0;
		unsigned int locKind = GetSignAbs(locTerm, locAbs);

		const auto& locListEntry = mbrWatchList[locAbs - 1];
		const auto& locEntryList = locListEntry.Watch[locKind];

		// If this term is another watched literal
		// locLit must be 0 here
		if (locEntryList.find(&argClause) != locEntryList.end())
			locLit = locTerm;
		// If the value is not false
		else if ((locTerm + locListEntry.Val) != 0)
			return locTerm;

		}

	return locLit;

	}



bool tsState::UpdateWatch(assignQueue& argAssignQueue,
                          assignSeq& argAssignSeq,
                          satClause& argLearntClause) {

	// Extract assignment
	satLit locAssign = argAssignQueue.front().first;
	auto   locAntec = argAssignQueue.front().second;

	argAssignQueue.pop_front();

	satLit       locAbs  = 0;
	unsigned int locKind = GetSignAbs(locAssign, locAbs);
	auto& locLitEntryToUpdate = mbrWatchList[locAbs - 1];

	// This could happen if two implication from two clauses are on the same literal
	if (locAssign == locLitEntryToUpdate.Val)
		return true;

	if (locLitEntryToUpdate.Val != 0) {

		argLearntClause = GenLearntClause(*locAntec, argAssignSeq);
		return false;

		}

	locLitEntryToUpdate.Val = locAssign;
	locLitEntryToUpdate.Antec = locAntec;
	argAssignSeq.emplace_back(locAssign);
	mbrFloppyVarSet.erase(&locLitEntryToUpdate);

	satLit locRevLit = -locAssign;
	unsigned int locRevKind = locKind ^ 1;
	auto& locLitListToUpdate = locLitEntryToUpdate.Watch[locRevKind];
	auto  locItClause = locLitListToUpdate.begin();

	while (locItClause != locLitListToUpdate.end()) {

		const auto& locClause = *(*(locItClause++));

		satLit locNewWatch = FindNextLitToWatch(locRevLit, locClause);

		// This can only happen if this clause is a unit clause at the first place
		if (locNewWatch == 0)
			return false;

		satLit       locNewAbs = 0;
		unsigned int locNewKind = GetSignAbs(locNewWatch, locNewAbs);

		auto& locListEntry = mbrWatchList[locNewAbs - 1];
		auto& locEntryList = locListEntry.Watch[locNewKind];

		// This is a watched literal
		if (locEntryList.find(&locClause) != locEntryList.end()) {

			// If not assigned yet
			if (locListEntry.Val == 0)
				argAssignQueue.emplace_back(locNewWatch, &locClause);
			// Conflict
			else if (locListEntry.Val != locNewWatch) {

				argLearntClause = GenLearntClause(locClause, argAssignSeq);
				return false;

				}

			}
		// This is a valid watch candidate
		else {

			// C++14 guarantees that removing an iterator won't clobber
			// other iterators. The order of other elements is also preserved.
			locLitListToUpdate.erase(&locClause);
			locEntryList.emplace(&locClause);

			}

		}

	return true;

	}



// First-UIP
satClause tsState::GenLearntClause(const satClause& argClause,
                                   const assignSeq& argAssignSeq) {

	satLit locLit = 0;
	const satClause* locAntec = nullptr;

	auto locLearntClause = argClause;
	auto locItAssign = argAssignSeq.rbegin();

	// How could it go wrong? /s
	while (locItAssign != argAssignSeq.rend()) {

		satLit locAssign = *(locItAssign++);

		if (locLearntClause.find(-locAssign) == locLearntClause.end())
			continue;

		if (locLit == 0) {

			locLit = locAssign;
			locAntec = mbrWatchList[GetAbs(locAssign) - 1].Antec;
			continue;

			}

		locLearntClause = Resolve(std::move(locLearntClause),
		                          *locAntec,
		                          locLit);

		locLit = 0;
		locAntec = nullptr;
		locItAssign = argAssignSeq.rbegin();

		}

	return locLearntClause;

	}



satSol toySolver::SolveImpl(const satExpr& argExpr) const {

	tsState locState(argExpr.NumOfVar());

	// Initial watch
	for (const auto& locClause : argExpr) {

		if ( ! locState.WatchClause(locClause) )
			return satSol();

		}

	satLit  locNextTry = locState.GetFloppyVar();

	while ( argExpr.IsValidLiteral(locNextTry) ) {

		satLit locAssignResult = locState.AssignValue(locNextTry, false);

		if (locAssignResult == std::numeric_limits<satLit>::min())
			return satSol();

		if (locAssignResult == 0) {

			locNextTry = locState.GetFloppyVar();
			continue;

			}

		// Failed
		locNextTry = locAssignResult;

		// Backtrack
		while (locNextTry < 0 && locState.mbrAssignLog.size()) {

			auto& locLastAssignSeq = locState.mbrAssignLog.back();

			locNextTry = locLastAssignSeq.front();
			locState.RevertAssignSeq(locLastAssignSeq);
			locState.mbrAssignLog.pop_back();

			}

		if (locNextTry > 0)
			locNextTry = -locNextTry;
		else
			return satSol();

		}

	satSol locSol;

	locSol.reserve(argExpr.NumOfVar());

	for (const auto& locEntry : locState.mbrWatchList)
		locSol.emplace_back(locEntry.Val);

	return locSol;

	}
